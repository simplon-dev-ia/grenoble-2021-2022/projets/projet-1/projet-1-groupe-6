# Projet 1 - Nutri-score
Prédiction de l'indicateur alimentaire nutri-score  

# Contexte du projet
L'équipe IA de l'association Open Food Facts fait appel à vous !  
Beaucoup de produits de leur base de données sont déjà annotés avec l'indicateur nutri-score, mais il en manque encore. Ils se demandent si cet indicateur ne pourrait pas être inféré automatiquement à partir des autres méta-données présentes sur chaque produit.  
Vous êtes en charge de développer un prototype d'application web permettant de prédire le nutri-score
d'un produit à partir d'une série de critères à définir.   

# Contenu des dossiers du dépôt
Le dépôt Gitlab contient :  
Un dossier `document` contenant les maquettes de conception de l'application et le schéma d'architecture technique.  
Un dossier `modèle` :  
        -`project_1_clean_data.ipynb` : ce notebook contient toute les étapes de nettoyage du jeu de données.  
        -`project_1_find_modele.ipynb` : ce notebook contient la recherche du modèle de machine learning.  
Un dossier `application` contenant tout ce qui est rapport à l'application.  

# Faire sa prédiction
Dans un notebook par example, chargez les modèles avec les commandes suivantes
```py 
model = load('xgbc.joblib') 
scaler = load("scaler_xgbc.joblib")
```
Dans une liste de liste, entrez vos valeurs dans l'ordre suivant :
- energy_100g
- fat_100g
- saturated-fat_100g
- carbohydrates_100g
- fiber_100g
- sugars_100g
- proteins_100g
- salt_100g

Voiçi un example :
```py
nouvelle_prediction = [[2341,23,17,12.9,4.2,5,7.5,1]]
```
Utilisez ces valeurs afin de faire une nouvelle prédiction de la façon suivante :
```py
a_predire = scaler.transform(nouvelle_prediction)
votre_prediction = model.predict(a_predire)
print(votre_prediction)
```
# L'application web
Lien vers l'application web : https://nutriscore-app.herokuapp.com/

# Présentation du projet
Lien du la présentation : https://docs.google.com/presentation/d/1ysU_-Sa7CxVSqF-eBXOtJDkJKOSuA5D41Y3uejZJGq0/edit?usp=sharing
